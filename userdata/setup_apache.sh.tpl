#!/bin/bash

apt-get update -y
apt-get install -y apache2 apache2-dev
systemctl start apache2
systemctl enable apache2
