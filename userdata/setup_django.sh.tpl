#!/bin/bash

home_dir="/home/ubuntu"
project_name="django_website"
main_app_name="blogsite"

cd $home_dir
apt-get update -y
apt-get install -y git python3-pip python3-venv apache2 apache2-dev libapache2-mod-wsgi-py3

public_ip=$(curl http://checkip.amazonaws.com)

sudo -u ubuntu bash -c "
python3 -m venv env
source env/bin/activate

git clone https://gitlab.com/fuiweq65/$project_name.git
cd $project_name

pip3 install -r requirements.txt
python3 manage.py collectstatic
deactivate
"

sed -i "s/ALLOWED_HOSTS = \[\".*/ALLOWED_HOSTS = [\"$public_ip\"]/" $project_name/$main_app_name/settings.py

chown :www-data $project_name/db.sqlite3
chmod 664 $project_name/db.sqlite3
chown :www-data $project_name
chmod 775 $project_name

echo "<VirtualHost *:80>
    ServerAdmin admin@$public_ip
    ServerName $public_ip
    # ServerAlias www.testdjango.localhost
    DocumentRoot $home_dir/$project_name
    ErrorLog \$\{APACHE_LOG_DIR}/error.log
    CustomLog \$\{APACHE_LOG_DIR}/access.log combined

    Alias /static $home_dir/$project_name/static
    <Directory $home_dir/$project_name/static>
        Require all granted
    </Directory>

    <Directory $home_dir/$project_name/$main_app_name>
        <Files wsgi.py>
            Require all granted
        </Files>
    </Directory>

    WSGIDaemonProcess django_project python-path=$home_dir/$project_name python-home=$home_dir/env
    WSGIProcessGroup django_project
    WSGIScriptAlias / $home_dir/$project_name/$main_app_name/wsgi.py
</VirtualHost>" > /etc/apache2/sites-available/000-default.conf
sed -i "s/\\\{APACHE_LOG_DIR/{APACHE_LOG_DIR/" /etc/apache2/sites-available/000-default.conf

systemctl enable apache2
systemctl restart apache2
