#!/usr/bin/env python

import argparse
import platform
import subprocess as sp

SYSTEM = platform.system()

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--region", "-region", help="AWS region to deploy to",
        choices=[
            "eu-north-1",
            "ap-south-1",
            "eu-west-3",
            "eu-west-2",
            "eu-west-1",
            "ap-northeast-3",
            "ap-northeast-2",
            "me-south-1",
            "ap-northeast-1",
            "sa-east-1",
            "ca-central-1",
            "ap-east-1",
            "ap-southeast-1",
            "ap-southeast-2",
            "eu-central-1",
            "us-east-1",
            "us-east-2",
            "us-west-1",
            "us-west-2",
    ])
    parser.add_argument("--user-data", "-user-data", help="Name of user data script", choices=["nginx", "apache", "django"])
    parser.add_argument("--server-name", "-server-name", help="Name of the server instance")
    parser.add_argument("--debug", "-debug", help="Debug flag; terraform plan instead of apply", action="store_true")
    parser.add_argument("--auto", "-auto", "-y", help="Set apply or destroy to non-interactive mode", action="store_true")
    parser.add_argument("--destroy", "-destroy", "-d", help="Execute terraform destroy", action="store_true")

    args = parser.parse_args()
    region      = args.region if args.region != None else ""
    user_data   = args.user_data if args.user_data != None else ""
    server_name = args.server_name if args.server_name != None else ""
    debug       = args.debug
    auto        = args.auto
    destroy     = args.destroy

    # terraform strings
    tf_plan    = "terraform plan"
    tf_apply   = "terraform apply"
    tf_destroy = "terraform destroy"

    # Set terraform variables
    tf_var_str = ""
    if region:
        tf_var_str += f" -var \"aws_region={region}\""
    if user_data:
        tf_var_str += f" -var \"user_data_script={user_data}\""
    if server_name:
        tf_var_str += f" -var \"instance_name={server_name}\""

    # Compose terraform string
    tf_str = ""
    if destroy:
        tf_str = tf_destroy
    elif debug:
        tf_str = tf_plan
    else:
        tf_str = tf_apply

    # Add variables
    tf_str += tf_var_str

    if auto:
        tf_str += " --auto-approve"

    sp.Popen(tf_str, shell=True).wait()

if __name__ == "__main__":
    main()