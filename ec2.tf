data "aws_ami" "this" {
  most_recent = true

  filter {
      name   = "name"
      values = ["ubuntu/images/hvm-ssd/ubuntu-*-20.04-amd64-server-*"]
  }

  filter {
      name   = "virtualization-type"
      values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical

}

resource "aws_iam_role" "this" {
  name = "ssm_iam_role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })
}

resource "aws_iam_role_policy_attachment" "this" {
  count      = 1
  role       = aws_iam_role.this.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
}

resource "aws_iam_instance_profile" "this" {
  name = "ssm_iam_instance_profile"
  role = aws_iam_role.this.name
}


resource "aws_network_interface" "server" {
  subnet_id       = module.vpc.public_subnets[0]
  security_groups = [ aws_security_group.server_sg.id ]
}

resource "aws_instance" "server" {
  ami           = data.aws_ami.this.id
  instance_type = "t2.micro"

  network_interface {
    network_interface_id = aws_network_interface.server.id
    device_index         = 0
  }
  iam_instance_profile = aws_iam_instance_profile.this.id

  # Run userdata script
  user_data = var.user_data_script != "" ? templatefile("${path.module}/userdata/setup_${var.user_data_script}.sh.tpl", {}) : ""

  tags = {
    Name = "${var.instance_name}"
    User_data = "${var.user_data_script}"
  }
}
