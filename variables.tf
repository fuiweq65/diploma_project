variable "aws_region" {
    default = "eu-central-1"
    description = "Region to deploy to"
}

variable "user_data_script" {
    default = ""
    description = "Name of script to use for user data on instance provision"
}

variable "instance_name" {
    default = "webserver-instance"
    description = "Name of the instance"
}