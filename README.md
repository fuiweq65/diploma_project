## Requirements
To use this code you must first setup an AWS account, install and configure ```aws cli``` and ```terraform```. If you don't configure these before running the script it will exit with an error.  
The script runs on any system that supports Terraform and Python.

## Usage
You can either run the Python scrpit or use Terraform directly. Using the script you can set different Terraform variables.

To get the list of available arguments run the script with the ```-h``` flag.  

Script usage examples:  
> ```python3 run_deploy.py -y``` &ndash; spin up empty instance  
> ```python3 run_deploy.py -y -d``` &ndash; delete all terraform infrastructure without asking for confirmation  
> ```python3 run_deploy.py -user-data django``` &ndash; spin up an instance configured to run a django server with an example website
> ```python3 run_deploy.py --server-name "My Webserver" -y``` &ndash; spin up an instance with the name tag set to "My Webserver" 