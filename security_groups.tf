resource "aws_security_group" "server_sg" {
  name = "server_sg"
  vpc_id = module.vpc.vpc_id
}

resource "aws_security_group_rule" "allow_http" {
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = [ "0.0.0.0/0" ]
  ipv6_cidr_blocks  = [ "::/0" ]
  security_group_id = aws_security_group.server_sg.id
}

resource "aws_security_group_rule" "allow_egress" {
  type              = "egress"
  from_port         = -1
  to_port           = -1
  protocol          = -1
  cidr_blocks       = [ "0.0.0.0/0" ]
  ipv6_cidr_blocks  = [ "::/0" ]
  security_group_id = aws_security_group.server_sg.id
}

resource "aws_security_group" "ssm_sg" {
  name = "ssm_sg"
  vpc_id = module.vpc.vpc_id
}

resource "aws_security_group_rule" "allow_https" {
  type              = "ingress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  security_group_id = aws_security_group.server_sg.id
  source_security_group_id = aws_security_group.server_sg.id
}